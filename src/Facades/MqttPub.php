<?php

namespace Negotica\MqttPub\Facades;

use Illuminate\Support\Facades\Facade;

class MqttPub extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'mqtt_pub';
    }
}
