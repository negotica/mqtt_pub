# mqtt_pub

Build:
```bash
docker build -t negotica/mqtt_pub docker
```

Run:
```bash
docker run -it --rm --init -p 6788:6788 negotica/mqtt_pub
```
