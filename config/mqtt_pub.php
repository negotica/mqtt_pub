<?php

return [

    'host' => env('MQTT_PUB_HOST', 'localhost'),
    'port' => env('MQTT_PUB_PORT', '6788'),
    'throw_exceptions' => true,

];
