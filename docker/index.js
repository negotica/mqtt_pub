require('dotenv').config();
const express = require('express');
const mqtt = require('mqtt');
const { body, validationResult } = require('express-validator');

const http_port = process.env.HTTP_PORT || 6788;
const mqtt_host = process.env.MQTT_HOST || 'localhost';
const mqtt_port = +process.env.MQTT_PORT || 1883;
const mqtt_username = process.env.MQTT_USERNAME || '';
const mqtt_password = process.env.MQTT_PASSWORD || '';
const mqtt_default_qos = +process.env.MQTT_DEFAULT_QOS || 0;
const mqtt_default_retain = process.env.MQTT_DEFAULT_RETAIN || false;

const http_server = express();
http_server.use(express.json());
http_server.use(express.urlencoded({ extended: true }));

const mqtt_client = mqtt.connect({
    host: mqtt_host,
    port: mqtt_port,
    clientId: 'mqtt_pub_' + Math.random().toString(16).substring(2, 8),
    reconnectPeriod: 1000,
    username: mqtt_username,
    password: mqtt_password,
});
mqtt_client.on('connect', () => console.log(`Mqtt connected to ${mqtt_host}:${mqtt_port}`));
mqtt_client.on('error', console.error);

http_server.post('/pub',
    body('topic').exists().isString().notEmpty().trim(),
    body('message').exists().isString().trim(),
    body('qos').optional().isIn([0, 1, 2]),
    body('retain').optional().isBoolean({ strict: true }),
    (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).send({ errors: errors.mapped() });
        }

        if (!mqtt_client.connected) {
            throw new Error('MQTT Client not connected');
        }

        const topic = req.body.topic;
        const message = req.body.message;
        const qos = +req.body.qos || mqtt_default_qos;
        const retain = req.body.retain || mqtt_default_retain;

        mqtt_client.publish(topic, message, { qos, retain }, (err) => {
            if (err) {
                return next(err);
            }
            res.json({});
        });
    });

http_server.all('/ping', (req, res) => {
    if (!mqtt_client.connected) {
        throw new Error('MQTT Client not connected');
    }
    res.send('ok');
});

http_server.use((err, req, res, next) => {
    res.status(500).json({ error: err.message, stack: err.stack });
});

http_server.listen(http_port, () => console.log(`Listening on port ${http_port}`));
