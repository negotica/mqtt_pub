<?php

namespace Negotica\MqttPub;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/mqtt_pub.php', 'mqtt_pub');

        $this->app->when(MqttPub::class)
            ->needs('$host')
            ->giveConfig('mqtt_pub.host');

        $this->app->when(MqttPub::class)
            ->needs('$port')
            ->giveConfig('mqtt_pub.port');

        $this->app->when(MqttPub::class)
            ->needs('$throw_exceptions')
            ->giveConfig('mqtt_pub.throw_exceptions');

        $this->app->bind('mqtt_pub', function ($app) {
            return $app->make(MqttPub::class);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config/mqtt_pub.php' => config_path('mqtt_pub.php'),
            ], 'config');
        }
    }
}
