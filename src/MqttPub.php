<?php

namespace Negotica\MqttPub;

use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;

class MqttPub
{
    private $client;

    public function __construct($host, $port, $throw_exceptions)
    {
        $this->client = Http::withOptions([
            'base_uri' => "http://$host:$port",
        ])->throwIf($throw_exceptions);
    }

    public function publish(string $topic, string|array $message, int $qos = null, bool $retain = null): Response
    {
        if (is_array($message)) {
            $message = json_encode($message);
        }

        return $this->client->post('/pub', array_merge(
            [
                'topic' => $topic,
                'message' => $message,
            ], array_filter([
                'qos' => $qos,
                'retain' => $retain,
            ])
        ));
    }
}
